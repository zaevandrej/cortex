﻿using System;
using System.Web;
using Cortex.Web.Models.Shared;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Cortex.Web.Helpers
{
    public static class Helpers
    {
        public static IHtmlContent DisplayForUser(this IHtmlHelper helper, UserDisplayModel model, Guid? userId)
        {
            string name = HttpUtility.HtmlEncode(model.Name);
            string username = HttpUtility.HtmlEncode(model.UserName.ToLower());

            string linkText = userId == model.Id ? "you" : $"{name} <{username}>";

            return helper.ActionLink(linkText, "GetUser", "Users", new { userName = username }, new { });
        }
    }
}
