﻿import 'font-awesome-webpack';

import './styles/main.less';
import './styles/networks.less';
import './styles/network-editor.less';
import './styles/log-in.less';

import './scripts/validation/init-validation';
import './scripts/accesses-editor/init';
import './scripts/network-editor';
import './scripts/local-datetime/init';
import './scripts/confirmation/init';