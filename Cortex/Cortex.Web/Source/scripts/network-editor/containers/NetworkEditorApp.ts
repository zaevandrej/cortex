﻿import * as React from 'react';
import { NetworkEditor, INetworkEditorProps } from '../components/NetworkEditor'
import { fetchNetwork, startEditing, receiveNetwork } from '../actions';
import { connect } from 'react-redux';
import { RootState } from '../models';
import { List } from 'immutable';

const mapStateToProps = (state: RootState): Partial<INetworkEditorProps> => ({
    versionId: state.versionId,
    isEdit: state.isEdit,
    isReadOnly: state.isReadOnly,
    isLoading: !state.isLoaded
});

const mapDispatchToProps = (dispatch): Partial<INetworkEditorProps> => ({
    onLoad: versionId => versionId ? dispatch(fetchNetwork(versionId)) : dispatch(receiveNetwork(List(), List())),
    onEdit: () => dispatch(startEditing())
});

const NetworkEditorApp = connect(mapStateToProps, mapDispatchToProps)(NetworkEditor);

export default NetworkEditorApp;