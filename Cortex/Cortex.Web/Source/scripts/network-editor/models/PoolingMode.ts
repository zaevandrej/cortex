﻿enum PoolingMode {
    Max = 0,
    Average = 1
}

export default PoolingMode;