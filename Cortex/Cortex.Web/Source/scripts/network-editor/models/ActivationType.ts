﻿enum ActivationType {
    Softmax = 1,
    ELU = 2,
    SELU = 3,
    Softplus = 4,
    Softsign = 5,
    ReLU = 6,
    tanh = 7,
    Sigmoid = 8,
    HardSigmoid = 9,
    Linear = 10,
    Other = 11
}

export default ActivationType;