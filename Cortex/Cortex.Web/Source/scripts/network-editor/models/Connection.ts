﻿import { Record } from 'immutable';

export interface IConnection {
    id: number;
    fromId: number;
    toId: number;
}

const ConnectionRecord = Record({ id: 0, fromId: 0, toId: 0 });

export class Connection extends ConnectionRecord implements IConnection {
    constructor(props: Partial<IConnection> = {}) {
        super(props);
    }
}

export interface INewConnection {
    connection: Connection,
    isValid: boolean
}

const NewConnectionRecord = Record<INewConnection>({ connection: new Connection(), isValid: false });

export class NewConnection extends NewConnectionRecord implements INewConnection {
    constructor(props: Partial<INewConnection> = {}) {
        super(props);
    }
}