﻿enum LayerType {
    Dense = 0,
    Convolutional = 1,
    Pooling = 2,
    Recurrent = 3
}

export default LayerType;