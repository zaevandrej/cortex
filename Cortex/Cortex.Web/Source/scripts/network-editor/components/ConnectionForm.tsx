﻿import * as React from 'react';
import { Connection, Layer } from '../models';
import { List } from 'immutable';
import { SelectInput, IOption } from './inputs';
import { NewConnection } from '../models/Connection';

export interface IConnectionFormProps {
    connection: Connection,
    layers: List<Layer>,
    connections: List<Connection>,
    onChange: (connection: NewConnection) => void
}

const ConnectionForm = (props: IConnectionFormProps) => {
    const layerOptions: List<IOption> = props.layers.map(l => ({ value: l.id, label: l.name }));

    function validate(connection: Connection): NewConnection {
        const isSameLayer = connection.fromId === connection.toId;
        const isExistingConnection = props.connections
            .some(c => c.fromId === connection.fromId && c.toId === connection.toId);

        return new NewConnection({ connection: connection, isValid: !isSameLayer && !isExistingConnection });
    }

    return (
        <div className="form">
            <SelectInput label="From"
                value={props.connection.fromId}
                options={layerOptions}
                onChange={v => props.onChange(validate(props.connection.set('fromId', v)))}
                isReadOnly={false} />
            <SelectInput label="To"
                value={props.connection.toId}
                options={layerOptions}
                onChange={v => props.onChange(validate(props.connection.set('toId', v)))}
                isReadOnly={false} />
        </div>);
};

export default ConnectionForm;