﻿import * as React from 'react';
import * as Modal from 'react-modal';
import { TextInput, SelectInput, CheckboxInput, NumericInput, MultilineInput } from './inputs';
import { List } from 'immutable';
import { IOption } from './inputs';
import { ActivationType, LayerType, PoolingMode, Layer } from '../models';
import LayerForm from './LayerForm';

export interface IAddLayerProps {
    isOpen: boolean;
    onClose: () => void;
    onSave: (layer: Layer) => void;
}

export interface IAddLayerState {
    readonly layer: Layer
}

export default class AddLayerDialog extends React.Component<IAddLayerProps, IAddLayerState> {
    private appElement = document.getElementById('network-editor');

    constructor(props) {
        super(props);

        this.save = this.save.bind(this);
        this.close = this.close.bind(this);

        this.state = {
            layer: this.getDefaultLayer()
        };
    }

    reset() {
        this.setState({ layer: this.getDefaultLayer() })
    }

    getDefaultLayer() {
        return new Layer({
            activation: ActivationType.ReLU,
            type: LayerType.Dense,
            poolingMode: PoolingMode.Average,
            kernelHeight: 1,
            kernelWidth: 1,
            kernelsNumber: 1,
            neuronsNumber: 1
        });
    }

    save() {
        this.props.onSave(this.state.layer);
        this.reset();
    }

    close() {
        this.props.onClose();
        this.reset();
    }

    public render() {
        return (
            <Modal isOpen={this.props.isOpen} appElement={this.appElement}>
                <div className="dialog-heading">
                    <h4>Add Layer</h4>
                </div>
                <div className="dialog-body">
                    <LayerForm layer={this.state.layer} onChange={l => this.setState({ layer: l })} isReadOnly={false} />
                </div>
                <div className="dialog-buttons">
                    <button className="button" onClick={this.close}>Cancel</button>
                    <button className="button-primary" onClick={this.save}>Add</button>
                </div>
            </Modal>
        );
    }
}
