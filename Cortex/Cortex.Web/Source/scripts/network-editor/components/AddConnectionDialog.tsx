﻿import * as React from 'react';
import * as Modal from 'react-modal';
import { Connection } from '../models';
import { Record } from 'immutable';
import ValidatedConnectionForm from '../containers/ValidatedConnectionForm';
import { NewConnection } from '../models/Connection';


export interface IAddConnectionDialogProps {
    isOpen: boolean;
    onClose: () => void;
    onSave: (connection: Connection) => void;
}

interface IAddConnectionDialogState {
    connection: Connection,
    isValid: boolean
}

const AddConnectionDialogStateRecord = Record<IAddConnectionDialogState>({ connection: null, isValid: false });

class AddConnectionDialogState extends AddConnectionDialogStateRecord {
    constructor(state: Partial<IAddConnectionDialogState> = {}) {
        super(state);
    }
}

export default class AddConnectionDialog
    extends React.Component<IAddConnectionDialogProps, { record: AddConnectionDialogState }> {
    private appElement = document.getElementById('network-editor');

    constructor(props) {
        super(props);

        this.updateConnection = this.updateConnection.bind(this);

        this.state = { record: new AddConnectionDialogState() };
    }

    updateConnection(connection: NewConnection) {
        this.setState(prevState => ({
            record: prevState.record.set('isValid', connection.isValid)
                .set('connection', connection.connection)
        }));
    }

    public render() {
        return (
            <Modal isOpen={this.props.isOpen} appElement={this.appElement}>
                <div className="dialog-heading">
                    <h4>Add Connection</h4>
                </div>
                <div className="dialog-body">
                    <ValidatedConnectionForm connection={this.state.record.connection}
                        onChange={c => this.updateConnection(c)} />
                </div>
                <div className="dialog-buttons">
                    <button className="button" onClick={this.props.onClose}>Cancel</button>
                    <button className="button-primary"
                        onClick={() => this.props.onSave(this.state.record.connection)}
                        disabled={!this.state.record.isValid}>
                        Add
                    </button>
                </div>
            </Modal>
        );
    }
}
