﻿export default class UserDto {
    public id: string;
    public userName: string;
    public name: string;
}