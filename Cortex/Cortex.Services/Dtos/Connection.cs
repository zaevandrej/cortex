﻿namespace Cortex.Services.Dtos
{
    public class Connection
    {
        public int Id { get; set; }

        public int FromId { get; set; }

        public int ToId { get; set; }
    }
}