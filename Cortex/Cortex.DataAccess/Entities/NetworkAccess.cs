﻿using System;

namespace Cortex.DataAccess.Entities
{
    public class NetworkAccess
    {
        public Guid Id { get; set; }

        public AccessMode AccessMode { get; set; }
    }
}
