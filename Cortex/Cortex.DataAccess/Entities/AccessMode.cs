﻿namespace Cortex.DataAccess.Entities
{
    public enum AccessMode
    {
        Private,
        ByPermission,
        Public
    }
}
