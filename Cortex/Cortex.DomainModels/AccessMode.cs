﻿namespace Cortex.DomainModels
{
    public enum AccessMode
    {
        Private,
        ByPermission,
        Public
    }
}
